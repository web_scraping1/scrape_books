# scrape_books

I am learning web scraping. This is the first of many projects

## Installation

Clone the repository and install requirements

```bash
# clone with ssh
git clone git@gitlab.com:web_scraping1/scrape_books.git
cd scrape_books
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### learning resources
I am creating this project while learning from this video

[Web Scraping to CSV | Multiple Pages Scraping with BeautifulSoup](https://www.youtube.com/watch?v=MH3641s3Roc)
